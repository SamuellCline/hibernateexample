package hbntexample;
import javax.persistence.*;
@Entity
@Table(name = "Motorcycles")
public class Motorcycle {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
        private int ID;
    @Column(name = "Year")
        private int Year;
    @Column(name = "Make")
        private String Make;
    @Column(name = "Model")
        private String Model;
    @Column(name = "Size")
        private int Size;






    public int getId() {
        return ID;
    }
    public void setID(int ID){
            this.ID = ID;
    }
    public int getYear(){
            return Year;
    }
    public void setYear(int Year){
            this.Year = Year;
    }
    public String getMake(){
        return Make;
    }
    public void setMake(String Make){
        this.Make = Make;
    }
    public String getModel(){
        return Model;
    }
    public void setModel(String Model){
        this.Model = Model;
    }
    public int getSize(){
        return Size;
    }
    public void setSize(int Size){
        this.Size = Size;
    }



    public String toString() {
        return "Year: " + Year + System.getProperty("line.separator") + "Make: " + Make + System.getProperty("line.separator") + "Model: " + Model + System.getProperty("line.separator") + "Engine Displacement: " + Size;
    }


}
