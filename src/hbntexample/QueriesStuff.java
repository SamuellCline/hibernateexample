package hbntexample;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.time.Year;
import java.util.*;


public class QueriesStuff {

    SessionFactory factory = null;
    Session session = null;

    private static QueriesStuff single_instance = null;

    private QueriesStuff() {
        factory = HibernateUtilities.getSessionFactory();
    }


    public static QueriesStuff getInstance() {
        if (single_instance == null) {
            single_instance = new QueriesStuff();
        }

        return single_instance;
    }

    //lists all motorcycles
    public List<Motorcycle> getMotorcycles() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from hbntexample.Motorcycle";
            List<Motorcycle> cs = (List<Motorcycle>) session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    //Gets single motorcycle by ID
    public Motorcycle getMotorcycle(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from hbntexample.Motorcycle where id=" + Integer.toString(id);
            Motorcycle m = (Motorcycle) session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return m;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public Motorcycle addMotorcycle(int id, int Y, String M, String Mo, int S) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            Integer q = null;
           // String sql = "insert into hbntexample.Motorcycle (" + id + "," + Y + "," + M + "," + Mo + "," + S + ")";
           // Motorcycle m = (Motorcycle) session.createQuery(sql).getSingleResult();
            Motorcycle mc = new Motorcycle();
            mc.setID(id);
            mc.setYear(Y);
            mc.setMake(M);
            mc.setModel(Mo);
            mc.setSize(S);
            q = (Integer) session.save(mc);

            session.getTransaction().commit();
            return mc;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        }
        finally
        {
            session.close();
        }

    }
}
