package hbntexample;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Scanner;

public class DoingStuff {

    QueriesStuff q = QueriesStuff.getInstance();



    //lists all motorcycles
    public void listAll() {
        List<Motorcycle> m = q.getMotorcycles();
        for (Motorcycle i : m) {
            System.out.println(i);
        }
    }

    public void listByID(int x) {
        try {
            System.out.println(q.getMotorcycle(x));
        }
        catch (NoResultException e){
            System.out.println(e);
            System.out.println("No result found");

        }
    }
    public void addNew(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter ID here:");
        int x = input.nextInt();
        System.out.println("Enter Year here:");
        int y = input.nextInt();
        System.out.println("Enter Make here:");
        String m = input.next();
        System.out.println("Enter Model here:");
        String mo = input.next();
        System.out.println("Enter Engine Displacement here:");
        int s = input.nextInt();

    q.addMotorcycle(x, y, m, mo, s);
    }
}
