package hbntexample;
import java.util.*;
import java.util.Scanner;
public class Order66 {

        public static void main(String[] args) {

            boolean run = true;


           while(run){
            try {
                Scanner input = new Scanner(System.in);
                DoingStuff d = new DoingStuff();
                System.out.println("What would you like to do?");
                System.out.println("1.Query entire Table");
                System.out.println("2.Query By ID");
                System.out.println("3.Insert into Table");
                System.out.println("4.Exit");
                System.out.println("Please enter selection: ");
                int x = input.nextInt();

                switch(x) {
                    case 1:
                        d.listAll();
                        break;
                    case 2:
                        System.out.println("Please enter ID: ");
                        int v = input.nextInt();
                        d.listByID(v);

                        break;
                    case 3:
                        d.addNew();
                        break;
                    case 4:
                        run = false;
                        break;
                    default:
                       d.listAll();
                }
            }
            catch (InputMismatchException e){
                System.out.println(e);
            }
           }



        }
    }

